# Skip-List
This repository contains two parts.
## 1. The Original Source Code
> An efficient implementation of the Skip-List data structure in OCaml used in the _Tezos protocol_.

The original source code is extracted from [tezos/src/proto_alpha/lib_protocol/skip_list_repr.ml](https://gitlab.com/tezos/tezos/-/blob/master/src/proto_alpha/lib_protocol/skip_list_repr.ml) and then isolated here in [./lib](lib/skip_list_repr.ml).

## 2. Coq Model
An equivalent implementation of the _skip-list_ in Coq is presented in [./theories/SkipList.v](theories/SkipList.v).  
The goal is to prove formally this model against the original implementation.

## Install Dependencies
`opam repo add coq-released https://coq.inria.fr/opam/released`  
`opam install . --deps-only`  

## Build
`dune build`

## Test
`dune runtest`  
`dune build runtest --force` to force building & testing

