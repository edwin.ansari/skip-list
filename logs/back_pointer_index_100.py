In [9]: for idx in range(100):
   ...:     print("back_pointers_index_{}:".format(idx))
   ...:     print("  ",end="")
   ...:     for i in range(7):
   ...:         print("ptr_{0} -> [{1}], ".format(i,idx - (idx % 2**i)-1),end="")
   ...:     print("\n")
   ...: 
   ...: 
back_pointers_index_0:
  ptr_0 -> [-1], ptr_1 -> [-1], ptr_2 -> [-1], ptr_3 -> [-1], ptr_4 -> [-1], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_1:
  ptr_0 -> [0], ptr_1 -> [-1], ptr_2 -> [-1], ptr_3 -> [-1], ptr_4 -> [-1], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_2:
  ptr_0 -> [1], ptr_1 -> [1], ptr_2 -> [-1], ptr_3 -> [-1], ptr_4 -> [-1], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_3:
  ptr_0 -> [2], ptr_1 -> [1], ptr_2 -> [-1], ptr_3 -> [-1], ptr_4 -> [-1], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_4:
  ptr_0 -> [3], ptr_1 -> [3], ptr_2 -> [3], ptr_3 -> [-1], ptr_4 -> [-1], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_5:
  ptr_0 -> [4], ptr_1 -> [3], ptr_2 -> [3], ptr_3 -> [-1], ptr_4 -> [-1], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_6:
  ptr_0 -> [5], ptr_1 -> [5], ptr_2 -> [3], ptr_3 -> [-1], ptr_4 -> [-1], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_7:
  ptr_0 -> [6], ptr_1 -> [5], ptr_2 -> [3], ptr_3 -> [-1], ptr_4 -> [-1], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_8:
  ptr_0 -> [7], ptr_1 -> [7], ptr_2 -> [7], ptr_3 -> [7], ptr_4 -> [-1], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_9:
  ptr_0 -> [8], ptr_1 -> [7], ptr_2 -> [7], ptr_3 -> [7], ptr_4 -> [-1], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_10:
  ptr_0 -> [9], ptr_1 -> [9], ptr_2 -> [7], ptr_3 -> [7], ptr_4 -> [-1], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_11:
  ptr_0 -> [10], ptr_1 -> [9], ptr_2 -> [7], ptr_3 -> [7], ptr_4 -> [-1], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_12:
  ptr_0 -> [11], ptr_1 -> [11], ptr_2 -> [11], ptr_3 -> [7], ptr_4 -> [-1], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_13:
  ptr_0 -> [12], ptr_1 -> [11], ptr_2 -> [11], ptr_3 -> [7], ptr_4 -> [-1], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_14:
  ptr_0 -> [13], ptr_1 -> [13], ptr_2 -> [11], ptr_3 -> [7], ptr_4 -> [-1], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_15:
  ptr_0 -> [14], ptr_1 -> [13], ptr_2 -> [11], ptr_3 -> [7], ptr_4 -> [-1], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_16:
  ptr_0 -> [15], ptr_1 -> [15], ptr_2 -> [15], ptr_3 -> [15], ptr_4 -> [15], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_17:
  ptr_0 -> [16], ptr_1 -> [15], ptr_2 -> [15], ptr_3 -> [15], ptr_4 -> [15], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_18:
  ptr_0 -> [17], ptr_1 -> [17], ptr_2 -> [15], ptr_3 -> [15], ptr_4 -> [15], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_19:
  ptr_0 -> [18], ptr_1 -> [17], ptr_2 -> [15], ptr_3 -> [15], ptr_4 -> [15], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_20:
  ptr_0 -> [19], ptr_1 -> [19], ptr_2 -> [19], ptr_3 -> [15], ptr_4 -> [15], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_21:
  ptr_0 -> [20], ptr_1 -> [19], ptr_2 -> [19], ptr_3 -> [15], ptr_4 -> [15], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_22:
  ptr_0 -> [21], ptr_1 -> [21], ptr_2 -> [19], ptr_3 -> [15], ptr_4 -> [15], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_23:
  ptr_0 -> [22], ptr_1 -> [21], ptr_2 -> [19], ptr_3 -> [15], ptr_4 -> [15], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_24:
  ptr_0 -> [23], ptr_1 -> [23], ptr_2 -> [23], ptr_3 -> [23], ptr_4 -> [15], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_25:
  ptr_0 -> [24], ptr_1 -> [23], ptr_2 -> [23], ptr_3 -> [23], ptr_4 -> [15], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_26:
  ptr_0 -> [25], ptr_1 -> [25], ptr_2 -> [23], ptr_3 -> [23], ptr_4 -> [15], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_27:
  ptr_0 -> [26], ptr_1 -> [25], ptr_2 -> [23], ptr_3 -> [23], ptr_4 -> [15], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_28:
  ptr_0 -> [27], ptr_1 -> [27], ptr_2 -> [27], ptr_3 -> [23], ptr_4 -> [15], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_29:
  ptr_0 -> [28], ptr_1 -> [27], ptr_2 -> [27], ptr_3 -> [23], ptr_4 -> [15], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_30:
  ptr_0 -> [29], ptr_1 -> [29], ptr_2 -> [27], ptr_3 -> [23], ptr_4 -> [15], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_31:
  ptr_0 -> [30], ptr_1 -> [29], ptr_2 -> [27], ptr_3 -> [23], ptr_4 -> [15], ptr_5 -> [-1], ptr_6 -> [-1], 

back_pointers_index_32:
  ptr_0 -> [31], ptr_1 -> [31], ptr_2 -> [31], ptr_3 -> [31], ptr_4 -> [31], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_33:
  ptr_0 -> [32], ptr_1 -> [31], ptr_2 -> [31], ptr_3 -> [31], ptr_4 -> [31], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_34:
  ptr_0 -> [33], ptr_1 -> [33], ptr_2 -> [31], ptr_3 -> [31], ptr_4 -> [31], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_35:
  ptr_0 -> [34], ptr_1 -> [33], ptr_2 -> [31], ptr_3 -> [31], ptr_4 -> [31], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_36:
  ptr_0 -> [35], ptr_1 -> [35], ptr_2 -> [35], ptr_3 -> [31], ptr_4 -> [31], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_37:
  ptr_0 -> [36], ptr_1 -> [35], ptr_2 -> [35], ptr_3 -> [31], ptr_4 -> [31], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_38:
  ptr_0 -> [37], ptr_1 -> [37], ptr_2 -> [35], ptr_3 -> [31], ptr_4 -> [31], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_39:
  ptr_0 -> [38], ptr_1 -> [37], ptr_2 -> [35], ptr_3 -> [31], ptr_4 -> [31], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_40:
  ptr_0 -> [39], ptr_1 -> [39], ptr_2 -> [39], ptr_3 -> [39], ptr_4 -> [31], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_41:
  ptr_0 -> [40], ptr_1 -> [39], ptr_2 -> [39], ptr_3 -> [39], ptr_4 -> [31], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_42:
  ptr_0 -> [41], ptr_1 -> [41], ptr_2 -> [39], ptr_3 -> [39], ptr_4 -> [31], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_43:
  ptr_0 -> [42], ptr_1 -> [41], ptr_2 -> [39], ptr_3 -> [39], ptr_4 -> [31], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_44:
  ptr_0 -> [43], ptr_1 -> [43], ptr_2 -> [43], ptr_3 -> [39], ptr_4 -> [31], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_45:
  ptr_0 -> [44], ptr_1 -> [43], ptr_2 -> [43], ptr_3 -> [39], ptr_4 -> [31], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_46:
  ptr_0 -> [45], ptr_1 -> [45], ptr_2 -> [43], ptr_3 -> [39], ptr_4 -> [31], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_47:
  ptr_0 -> [46], ptr_1 -> [45], ptr_2 -> [43], ptr_3 -> [39], ptr_4 -> [31], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_48:
  ptr_0 -> [47], ptr_1 -> [47], ptr_2 -> [47], ptr_3 -> [47], ptr_4 -> [47], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_49:
  ptr_0 -> [48], ptr_1 -> [47], ptr_2 -> [47], ptr_3 -> [47], ptr_4 -> [47], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_50:
  ptr_0 -> [49], ptr_1 -> [49], ptr_2 -> [47], ptr_3 -> [47], ptr_4 -> [47], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_51:
  ptr_0 -> [50], ptr_1 -> [49], ptr_2 -> [47], ptr_3 -> [47], ptr_4 -> [47], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_52:
  ptr_0 -> [51], ptr_1 -> [51], ptr_2 -> [51], ptr_3 -> [47], ptr_4 -> [47], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_53:
  ptr_0 -> [52], ptr_1 -> [51], ptr_2 -> [51], ptr_3 -> [47], ptr_4 -> [47], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_54:
  ptr_0 -> [53], ptr_1 -> [53], ptr_2 -> [51], ptr_3 -> [47], ptr_4 -> [47], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_55:
  ptr_0 -> [54], ptr_1 -> [53], ptr_2 -> [51], ptr_3 -> [47], ptr_4 -> [47], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_56:
  ptr_0 -> [55], ptr_1 -> [55], ptr_2 -> [55], ptr_3 -> [55], ptr_4 -> [47], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_57:
  ptr_0 -> [56], ptr_1 -> [55], ptr_2 -> [55], ptr_3 -> [55], ptr_4 -> [47], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_58:
  ptr_0 -> [57], ptr_1 -> [57], ptr_2 -> [55], ptr_3 -> [55], ptr_4 -> [47], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_59:
  ptr_0 -> [58], ptr_1 -> [57], ptr_2 -> [55], ptr_3 -> [55], ptr_4 -> [47], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_60:
  ptr_0 -> [59], ptr_1 -> [59], ptr_2 -> [59], ptr_3 -> [55], ptr_4 -> [47], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_61:
  ptr_0 -> [60], ptr_1 -> [59], ptr_2 -> [59], ptr_3 -> [55], ptr_4 -> [47], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_62:
  ptr_0 -> [61], ptr_1 -> [61], ptr_2 -> [59], ptr_3 -> [55], ptr_4 -> [47], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_63:
  ptr_0 -> [62], ptr_1 -> [61], ptr_2 -> [59], ptr_3 -> [55], ptr_4 -> [47], ptr_5 -> [31], ptr_6 -> [-1], 

back_pointers_index_64:
  ptr_0 -> [63], ptr_1 -> [63], ptr_2 -> [63], ptr_3 -> [63], ptr_4 -> [63], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_65:
  ptr_0 -> [64], ptr_1 -> [63], ptr_2 -> [63], ptr_3 -> [63], ptr_4 -> [63], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_66:
  ptr_0 -> [65], ptr_1 -> [65], ptr_2 -> [63], ptr_3 -> [63], ptr_4 -> [63], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_67:
  ptr_0 -> [66], ptr_1 -> [65], ptr_2 -> [63], ptr_3 -> [63], ptr_4 -> [63], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_68:
  ptr_0 -> [67], ptr_1 -> [67], ptr_2 -> [67], ptr_3 -> [63], ptr_4 -> [63], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_69:
  ptr_0 -> [68], ptr_1 -> [67], ptr_2 -> [67], ptr_3 -> [63], ptr_4 -> [63], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_70:
  ptr_0 -> [69], ptr_1 -> [69], ptr_2 -> [67], ptr_3 -> [63], ptr_4 -> [63], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_71:
  ptr_0 -> [70], ptr_1 -> [69], ptr_2 -> [67], ptr_3 -> [63], ptr_4 -> [63], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_72:
  ptr_0 -> [71], ptr_1 -> [71], ptr_2 -> [71], ptr_3 -> [71], ptr_4 -> [63], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_73:
  ptr_0 -> [72], ptr_1 -> [71], ptr_2 -> [71], ptr_3 -> [71], ptr_4 -> [63], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_74:
  ptr_0 -> [73], ptr_1 -> [73], ptr_2 -> [71], ptr_3 -> [71], ptr_4 -> [63], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_75:
  ptr_0 -> [74], ptr_1 -> [73], ptr_2 -> [71], ptr_3 -> [71], ptr_4 -> [63], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_76:
  ptr_0 -> [75], ptr_1 -> [75], ptr_2 -> [75], ptr_3 -> [71], ptr_4 -> [63], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_77:
  ptr_0 -> [76], ptr_1 -> [75], ptr_2 -> [75], ptr_3 -> [71], ptr_4 -> [63], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_78:
  ptr_0 -> [77], ptr_1 -> [77], ptr_2 -> [75], ptr_3 -> [71], ptr_4 -> [63], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_79:
  ptr_0 -> [78], ptr_1 -> [77], ptr_2 -> [75], ptr_3 -> [71], ptr_4 -> [63], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_80:
  ptr_0 -> [79], ptr_1 -> [79], ptr_2 -> [79], ptr_3 -> [79], ptr_4 -> [79], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_81:
  ptr_0 -> [80], ptr_1 -> [79], ptr_2 -> [79], ptr_3 -> [79], ptr_4 -> [79], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_82:
  ptr_0 -> [81], ptr_1 -> [81], ptr_2 -> [79], ptr_3 -> [79], ptr_4 -> [79], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_83:
  ptr_0 -> [82], ptr_1 -> [81], ptr_2 -> [79], ptr_3 -> [79], ptr_4 -> [79], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_84:
  ptr_0 -> [83], ptr_1 -> [83], ptr_2 -> [83], ptr_3 -> [79], ptr_4 -> [79], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_85:
  ptr_0 -> [84], ptr_1 -> [83], ptr_2 -> [83], ptr_3 -> [79], ptr_4 -> [79], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_86:
  ptr_0 -> [85], ptr_1 -> [85], ptr_2 -> [83], ptr_3 -> [79], ptr_4 -> [79], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_87:
  ptr_0 -> [86], ptr_1 -> [85], ptr_2 -> [83], ptr_3 -> [79], ptr_4 -> [79], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_88:
  ptr_0 -> [87], ptr_1 -> [87], ptr_2 -> [87], ptr_3 -> [87], ptr_4 -> [79], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_89:
  ptr_0 -> [88], ptr_1 -> [87], ptr_2 -> [87], ptr_3 -> [87], ptr_4 -> [79], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_90:
  ptr_0 -> [89], ptr_1 -> [89], ptr_2 -> [87], ptr_3 -> [87], ptr_4 -> [79], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_91:
  ptr_0 -> [90], ptr_1 -> [89], ptr_2 -> [87], ptr_3 -> [87], ptr_4 -> [79], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_92:
  ptr_0 -> [91], ptr_1 -> [91], ptr_2 -> [91], ptr_3 -> [87], ptr_4 -> [79], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_93:
  ptr_0 -> [92], ptr_1 -> [91], ptr_2 -> [91], ptr_3 -> [87], ptr_4 -> [79], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_94:
  ptr_0 -> [93], ptr_1 -> [93], ptr_2 -> [91], ptr_3 -> [87], ptr_4 -> [79], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_95:
  ptr_0 -> [94], ptr_1 -> [93], ptr_2 -> [91], ptr_3 -> [87], ptr_4 -> [79], ptr_5 -> [63], ptr_6 -> [63], 

back_pointers_index_96:
  ptr_0 -> [95], ptr_1 -> [95], ptr_2 -> [95], ptr_3 -> [95], ptr_4 -> [95], ptr_5 -> [95], ptr_6 -> [63], 

back_pointers_index_97:
  ptr_0 -> [96], ptr_1 -> [95], ptr_2 -> [95], ptr_3 -> [95], ptr_4 -> [95], ptr_5 -> [95], ptr_6 -> [63], 

back_pointers_index_98:
  ptr_0 -> [97], ptr_1 -> [97], ptr_2 -> [95], ptr_3 -> [95], ptr_4 -> [95], ptr_5 -> [95], ptr_6 -> [63], 

back_pointers_index_99:
  ptr_0 -> [98], ptr_1 -> [97], ptr_2 -> [95], ptr_3 -> [95], ptr_4 -> [95], ptr_5 -> [95], ptr_6 -> [63], 
