In [8]: for idx in range(20):
   ...:     print("back_pointers_index_{}:".format(idx))
   ...:     print("  ",end="")
   ...:     for i in range(6):
   ...:         print("ptr_{0} -> [{1}], ".format(i,idx - (idx % 2**i)-1),end="")
   ...:     print("\n")
   ...: 
   ...: 
back_pointers_index_0:
  ptr_0 -> [-1], ptr_1 -> [-1], ptr_2 -> [-1], ptr_3 -> [-1], ptr_4 -> [-1], ptr_5 -> [-1], 

back_pointers_index_1:
  ptr_0 -> [0], ptr_1 -> [-1], ptr_2 -> [-1], ptr_3 -> [-1], ptr_4 -> [-1], ptr_5 -> [-1], 

back_pointers_index_2:
  ptr_0 -> [1], ptr_1 -> [1], ptr_2 -> [-1], ptr_3 -> [-1], ptr_4 -> [-1], ptr_5 -> [-1], 

back_pointers_index_3:
  ptr_0 -> [2], ptr_1 -> [1], ptr_2 -> [-1], ptr_3 -> [-1], ptr_4 -> [-1], ptr_5 -> [-1], 

back_pointers_index_4:
  ptr_0 -> [3], ptr_1 -> [3], ptr_2 -> [3], ptr_3 -> [-1], ptr_4 -> [-1], ptr_5 -> [-1], 

back_pointers_index_5:
  ptr_0 -> [4], ptr_1 -> [3], ptr_2 -> [3], ptr_3 -> [-1], ptr_4 -> [-1], ptr_5 -> [-1], 

back_pointers_index_6:
  ptr_0 -> [5], ptr_1 -> [5], ptr_2 -> [3], ptr_3 -> [-1], ptr_4 -> [-1], ptr_5 -> [-1], 

back_pointers_index_7:
  ptr_0 -> [6], ptr_1 -> [5], ptr_2 -> [3], ptr_3 -> [-1], ptr_4 -> [-1], ptr_5 -> [-1], 

back_pointers_index_8:
  ptr_0 -> [7], ptr_1 -> [7], ptr_2 -> [7], ptr_3 -> [7], ptr_4 -> [-1], ptr_5 -> [-1], 

back_pointers_index_9:
  ptr_0 -> [8], ptr_1 -> [7], ptr_2 -> [7], ptr_3 -> [7], ptr_4 -> [-1], ptr_5 -> [-1], 

back_pointers_index_10:
  ptr_0 -> [9], ptr_1 -> [9], ptr_2 -> [7], ptr_3 -> [7], ptr_4 -> [-1], ptr_5 -> [-1], 

back_pointers_index_11:
  ptr_0 -> [10], ptr_1 -> [9], ptr_2 -> [7], ptr_3 -> [7], ptr_4 -> [-1], ptr_5 -> [-1], 

back_pointers_index_12:
  ptr_0 -> [11], ptr_1 -> [11], ptr_2 -> [11], ptr_3 -> [7], ptr_4 -> [-1], ptr_5 -> [-1], 

back_pointers_index_13:
  ptr_0 -> [12], ptr_1 -> [11], ptr_2 -> [11], ptr_3 -> [7], ptr_4 -> [-1], ptr_5 -> [-1], 

back_pointers_index_14:
  ptr_0 -> [13], ptr_1 -> [13], ptr_2 -> [11], ptr_3 -> [7], ptr_4 -> [-1], ptr_5 -> [-1], 

back_pointers_index_15:
  ptr_0 -> [14], ptr_1 -> [13], ptr_2 -> [11], ptr_3 -> [7], ptr_4 -> [-1], ptr_5 -> [-1], 

back_pointers_index_16:
  ptr_0 -> [15], ptr_1 -> [15], ptr_2 -> [15], ptr_3 -> [15], ptr_4 -> [15], ptr_5 -> [-1], 

back_pointers_index_17:
  ptr_0 -> [16], ptr_1 -> [15], ptr_2 -> [15], ptr_3 -> [15], ptr_4 -> [15], ptr_5 -> [-1], 

back_pointers_index_18:
  ptr_0 -> [17], ptr_1 -> [17], ptr_2 -> [15], ptr_3 -> [15], ptr_4 -> [15], ptr_5 -> [-1], 

back_pointers_index_19:
  ptr_0 -> [18], ptr_1 -> [17], ptr_2 -> [15], ptr_3 -> [15], ptr_4 -> [15], ptr_5 -> [-1], 
