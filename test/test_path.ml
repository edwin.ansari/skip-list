open Skip_list
open Printf

module M = Skip_list_repr.Make (struct
  let basis = 2
end)

let _ = print_endline "[test_back_path]"

let rec show_list = function
  | None -> "None"
  | Some [] -> ""
  | Some [e] -> string_of_int e
  | Some (e :: l) -> string_of_int e ^ ", " ^ show_list (Some l)

let x : (int, int) M.cell = M.genesis 0

let a = Array.make 12 (M.genesis 0)

let rec return_list = function [Some x] :: xs -> x :: return_list xs | _ -> []

let _ =
  Array.iteri
    (fun i _ ->
      if i = 0 then ()
      else a.(i) <- M.next ~prev_cell:a.(i - 1) ~prev_cell_ptr:(i - 1) (i + 1))
    a ;
  let deref i = Some a.(i) in
  (*
     counter-examples for minimal path:
     cell_ptr, target_index
     {(6,3), (10,7), (10,5), ...}
  *)
  let cell_ptr = 10 and target_index = 5 in
  let bpath = M.back_path ~deref ~cell_ptr ~target_index in
  let bpath_str = show_list bpath in
  printf "back_pointers [%d]: [%s]\n" cell_ptr bpath_str ;
  printf "back_path (%d -> %d): [%s]\n" cell_ptr target_index (show_list bpath) ;
  Option.bind bpath @@ fun path ->
  printf
    "valid_path -> %b\n"
    (M.valid_back_path
       ~equal_ptr:( = )
       ~deref
       ~cell_ptr
       ~target_ptr:target_index
       path) ;
  None
