# Specification

## Interface
### Equal
```ml
  val equal :
    eq_cont: ('content -> 'content -> bool) ->
    eq_ptr: ('ptr -> 'ptr -> bool) ->
    cell: ('content, 'ptr) cell ->
    cell': ('content, 'ptr) cell ->
    bool
```
```coq
forall eq_cont, eq_ptr, cell, cell':
    (
    eq_cont cell.content cell'.contents /\
    length (cell.back_ptrs) = length (cell'.back_ptrs) /\
    (forall i in [0..(length cell.back_ptrs)]: 
        eq_ptr (cell.back_ptrs.(i)) (cell'.back_ptrs.(i)))
    )
    <=>
    equal eq_cont eq_ptr cell cell'
```

### Encoding
```ml
  val encoding :
    'ptr Data_encoding.t ->
    'content Data_encoding.t ->
    ('content, 'ptr) cell Data_encoding.t
```
```coq
  Admitted.
```

### Index
```ml
    val index: (_, _) cell -> int
```
```coq
    forall cell, index cell = cell.index
```

### Content
```ml
    val content : ('content, 'ptr) cell -> 'content
```
```coq
    forall cell, content cell = cell.content 
```

### Genesis
```ml
    val genesis : 'content -> ('content, 'ptr) cell
```
```coq
  forall cont, g = genesis cont =>
    content g = cont /\ index g = 0 /\ back_ptrs g = [None]
```

### Next
```ml
  (** [next ~prev_cell ~prev_cell_ptr content] creates a new cell
     that carries some [content], that follows [prev_cell]. *)
  val next :
    prev_cell:('content, 'ptr) cell ->
    prev_cell_ptr:'ptr ->
    'content ->
    ('content, 'ptr) cell
```
```coq
  forall prev_cell, prev_cell_ptr, cont:
    c = next prev_cell prev_cell_ptr cont =>
    (content c = cont) /\ (prev_cell_ptr in back_ptrs c) /\
    index c = index prev_cell + 1
```

### Back_path
```ml
  (** [back_path ~deref ~cell_ptr ~target_index] returns [Some path]
      where [path] is a sequence of back pointers to traverse to go
      from [cell_ptr] to the cell at position [target_index] in the
      sequence denoted by [(deref, cell_ptr)]. *)
    val back_path :
    deref:('ptr -> ('content, 'ptr) cell option) ->
    cell_ptr:'ptr ->
    target_index:int ->
    'ptr list option
```
```coq
```