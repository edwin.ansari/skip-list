Require Import SkipList.SkipList.
Require Import Lists.List.
Require Import Lia.
Require Import PeanoNat.
Require Import Bool.Bool.
Import ListNotations.

Set Implicit Arguments.

Section theorem.
Variables (content_t ptr_t : Type).

Notation cell_t := (cell content_t ptr_t).

(** invariant definitions *)

(* back_pointers[i] = Some (pointer to (index - (index mod (basis ** i)) - 1))
        (for all i < length back_pointers)
*)
Definition cell_bptrs_inv (c : cell_t) (deref : ptr_t -> cell_t)
  : Prop :=
    let bptrs := back_pointers c in
    let c_idx := index c in
      forall i : nat, (i < List.length bptrs) ->
        (back_pointer c i <> None) /\
        (forall bp, back_pointer c i = Some bp ->
          index (deref bp) =
            pointed_cell_index c_idx i).

(* length back_pointers = log basis index *)
Definition cell_bptrs_len_inv (c : cell_t)
  : Prop :=
    let bptrs := back_pointers c in
    let c_idx := index c in
      List.length bptrs = log_basis c_idx.

(** auxiliary functions *)

(* power p n <=> n = p ** k for some k *)
Inductive power (p : nat) : nat -> Prop :=
| Power_p_1 : power p 1
| Power_p_n n : power p n -> power p (p * n).

Lemma valid_power_pow :
  forall (p k : nat),
    power p (p ^ k).
Proof.
  intros p k.
  induction k.
  - cbn. apply Power_p_1.
  - apply Power_p_n in IHk.
    assumption.
Qed.

Lemma valid_power :
  forall p n : nat,
    power p n <-> exists k, Nat.pow p k = n.
Proof.
  intros p n.
  split; intros H.
  - induction H.
    + exists 0. cbn. reflexivity.
    + destruct IHpower as [k H1].
      exists (k + 1).
      rewrite Nat.add_1_r.
      rewrite Nat.pow_succ_r'.
      rewrite H1. reflexivity.
  - destruct H as [k H1].
    subst. apply valid_power_pow.
Qed.

(* valid_genesis proofs *)
Lemma valid_genesis_bptrs_len :
  forall ct : content_t,
    cell_bptrs_len_inv (genesis ct).
Proof.
  intros. unfold cell_bptrs_len_inv.
  cbn. reflexivity.
Qed.

Lemma valid_genesis_bptrs (deref : ptr_t -> cell_t) :
  forall ct : content_t,
    cell_bptrs_inv (genesis ct) deref.
Proof.
  unfold cell_bptrs_inv.
  cbn. intros ct i H. exfalso.
  apply (Nat.nlt_0_r i). assumption.
Qed.

Theorem valid_genesis :
  forall ct : content_t,
  let g : cell_t := genesis ct in
    content g = ct /\
    back_pointers g = List.nil /\
    index g = 0.
Proof.
  auto.
Qed.

(* valid_next proofs *)
Lemma valid_next_content :
  forall (prev_cell : cell_t) (prev_cell_ptr : ptr_t) (ct : content_t),
  let next_cell := next prev_cell prev_cell_ptr ct in
    content next_cell = ct.
Proof.
  auto.
Qed.

Lemma valid_next_index :
  forall (prev_cell : cell_t) (prev_cell_ptr : ptr_t) (ct : content_t),
  let next_cell := next prev_cell prev_cell_ptr ct in
    index next_cell = index prev_cell + 1.
Proof.
  auto.
Qed.

Lemma next_ptrs_len_succ_or_eq :
  forall (prev_cell : cell_t) (prev_cell_ptr : ptr_t) (ct : content_t),
  let next_cell := next prev_cell prev_cell_ptr ct in
  let len_prev := length (back_pointers prev_cell) in
  let len_next := length (back_pointers next_cell) in
    len_next = len_prev \/ len_next = len_prev + 1.
Proof.
  intros.
  cbn. unfold next_back_pointers'.
  (* case_eq
    next_index = b ** k -> len_next = len_prev
    otherwise -> len_next = len_prev + 1
  *)
Admitted.

Lemma valid_next_bptrs_len :
  forall (prev_cell : cell_t) (prev_cell_ptr : ptr_t) (ct : content_t),
  let next_cell := next prev_cell prev_cell_ptr ct in
    cell_bptrs_len_inv prev_cell ->
    cell_bptrs_len_inv next_cell.
Proof.
  intros.
  unfold cell_bptrs_len_inv in *.
  cbn. unfold next_back_pointers_safe.
Admitted.


(*
  Issues:
    - `deref` is not a parameter for `next`.
    - `deref` is maintained by the user.
    indeed it is needed for the `cell_bptrs_inv` proof.
    we need to suppose that `deref ptr` returns the valid pointed `cell`.

  Solutions:
    1. Oracle: define a virtual `deref` function
      with all valid bindings.

    2. define an `extends` relation:
        extends deref deref' next_cell :=
        {
          forall ptr, (deref ptr = c) -> (deref' ptr = c) /\
          exists ptr, (deref ptr = next_cell)
        }
*)
Lemma valid_next_bptrs (deref : ptr_t -> cell_t):
  forall (prev_cell : cell_t) (prev_cell_ptr : ptr_t) (ct : content_t),
    let next_cell := next prev_cell prev_cell_ptr ct in
    let next_bptrs := back_pointers next_cell in
      (List.length next_bptrs > 0) /\
      (cell_bptrs_inv prev_cell deref ->
      (* forall deref', extends deref deref' next_cell  -> *)
      (* cell_bptrs_inv next_cell deref'). *)
      cell_bptrs_inv next_cell deref).
Proof.
  intros prev_cell prev_cell_ptr ct next_cell next_bptrs.
  split.
  - cbn.
Admitted.

(* bptrs invariant on `next'` function *)
Lemma valid_next_bptrs' (deref : ptr_t -> cell_t):
  forall (prev_cell : cell_t) (prev_cell_ptr : ptr_t) (ct : content_t),
    let next_cell := next' prev_cell prev_cell_ptr ct in
    let next_bptrs := back_pointers next_cell in
      (List.length next_bptrs > 0) /\
      (cell_bptrs_inv prev_cell deref ->
      cell_bptrs_inv next_cell deref).
Proof.
  intros prev_cell prev_cell_ptr ct next_cell next_bptrs.
  split.
  - cbn. unfold next_back_pointers'. admit.
    (* apply next_ptrs_len_succ_or_eq & add_last increase len *)
  - intro H. unfold cell_bptrs_inv.
    intros i H0. split.
    + (* because next_cell_bptrs extracted from prev_cell *) admit.
    + intros. assert (index (deref bp) = index next_cell) by admit.
      rewrite H2. cbn. simpl. (* apply H?*) admit.
Admitted.

End theorem.
