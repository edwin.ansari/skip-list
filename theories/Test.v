Require Import Nat.
Require Import Arith.
Require Import Array.PArray.
Require Import Uint63.
Require Import ZArith.
Require Import Lists.List.
From Coq Require Import Bool.Bool.

Require Import SkipList.SkipList.
Import ListNotations.
Set Implicit Arguments.

(* PArray usage *)
(* Definition arr := (Array.PArray.make 4 (Some 5)).
Definition arr3 := arr.[0 <- Some 3].
Compute (arr3). *)

(* SkipList tests*)
(* Definition c1 {ptr_t} := SkipList.genesis (ptr_t := ptr_t) (2).
Definition c2 {ptr_t} := SkipList.genesis (ptr_t := ptr_t) (5).
Compute (SkipList.index c1 =? 0).
Compute (SkipList.content c1).
Compute ((SkipList.content c1) =? 2).

Compute (eqb (SkipList.equal Nat.eqb _ c1 c1) true).
Compute (eqb (SkipList.equal Nat.eqb _ c1 c2) false).

Compute (eq_nat (SkipList.log_basis 0) 0).
Compute (eq_nat (SkipList.log_basis 1) 0).
Compute (eq_nat (SkipList.log_basis 2) 1).
Compute (eq_nat (SkipList.log_basis 3) 2).
Compute (eq_nat (SkipList.log_basis 4) 2).
Compute (eq_nat (SkipList.log_basis 5) 3).
Compute (eq_nat (SkipList.log_basis 6) 3).
Compute (eq_nat (SkipList.log_basis 7) 3).
Compute (eq_nat (SkipList.log_basis 8) 3). *)

(* next tests *)
Example test_next :
  let c0 := SkipList.genesis 1 in 
  let c1 := SkipList.next c0 0 2 in
  let c2 := SkipList.next c1 1 3 in
  let c3 := SkipList.next c2 2 4 in
  let c4 := SkipList.next c3 3 5 in
  let c5 := SkipList.next c4 4 6 in
  let c6 := SkipList.next c5 5 7 in
  let c7 := SkipList.next c6 6 8 in
  let c8 := SkipList.next c7 7 8 in
  let c9 := SkipList.next c8 8 9 in 
  let c10 := SkipList.next c9 9 10 in 
  let c11 := SkipList.next c10 10 11 in
      c7 =  {| content := 8; back_pointers := [6; 5; 3]; index := 7 |}.
Proof. reflexivity. Qed.

(* next' tests *)
Example test_next' :
  let c0 := SkipList.genesis 1 in 
  let c1 := SkipList.next' c0 0 2 in
  let c2 := SkipList.next' c1 1 3 in
  let c3 := SkipList.next' c2 2 4 in
  let c4 := SkipList.next' c3 3 5 in
  let c5 := SkipList.next' c4 4 6 in
  let c6 := SkipList.next' c5 5 7 in
  let c7 := SkipList.next' c6 6 8 in
  let c8 := SkipList.next' c7 7 8 in
  let c9 := SkipList.next' c8 8 9 in 
  let c10 := SkipList.next' c9 9 10 in 
  let c11 := SkipList.next' c10 10 11 in
      c7 =  {| content := 8; back_pointers := [6; 5; 3]; index := 7 |}.
Proof. reflexivity. Qed.

Example test_next'_c11 :
  let c0 := SkipList.genesis 1 in 
  let c1 := SkipList.next' c0 0 2 in
  let c2 := SkipList.next' c1 1 3 in
  let c3 := SkipList.next' c2 2 4 in
  let c4 := SkipList.next' c3 3 5 in
  let c5 := SkipList.next' c4 4 6 in
  let c6 := SkipList.next' c5 5 7 in
  let c7 := SkipList.next' c6 6 8 in
  let c8 := SkipList.next' c7 7 8 in
  let c9 := SkipList.next' c8 8 9 in 
  let c10 := SkipList.next' c9 9 10 in 
  let c11 := SkipList.next' c10 10 11 in
    back_pointers c11 = [10; 9; 7; 7].
Proof. reflexivity. Qed.

(* back_path tests *)
Example test_bpath : 
  let c0 := SkipList.genesis 1 in 
  let c1 := SkipList.next c0 0 2 in
  let c2 := SkipList.next c1 1 3 in
  let c3 := SkipList.next c2 2 4 in
  let c4 := SkipList.next c3 3 5 in
  let c5 := SkipList.next c4 4 6 in
  let c6 := SkipList.next c5 5 7 in
  let c7 := SkipList.next c6 6 8 in
  let c8 := SkipList.next c7 7 8 in
  let c9 := SkipList.next c8 8 9 in 
  let c10 := SkipList.next c9 9 10 in 
  let c11 := SkipList.next c10 10 11 in
  let map_ptr := [c0;c1;c2;c3;c4;c5;c6;c7;c8;c9;c10;c11] in 
      SkipList.back_path (fun i => List.nth i map_ptr c1) 8 2 = Some [8; 7; 3; 2].
Proof. reflexivity. Qed.

(* valid_path tests *)
Example test_valid_bpath : 
  let c0 := SkipList.genesis 1 in 
  let c1 := SkipList.next c0 0 2 in
  let c2 := SkipList.next c1 1 3 in
  let c3 := SkipList.next c2 2 4 in
  let c4 := SkipList.next c3 3 5 in
  let c5 := SkipList.next c4 4 6 in
  let c6 := SkipList.next c5 5 7 in
  let c7 := SkipList.next c6 6 8 in
  let c8 := SkipList.next c7 7 8 in
  let c9 := SkipList.next c8 8 9 in 
  let c10 := SkipList.next c9 9 10 in 
  let c11 := SkipList.next c10 10 11 in
  let map_ptr := [c0;c1;c2;c3;c4;c5;c6;c7;c8;c9;c10;c11] in 
  let bpath := back_path (fun i => List.nth i map_ptr c0) 8 2 in
    match bpath with 
    | None => False 
    | Some bpath =>
      let vbpath := 
        valid_back_path
        (fun x y => x =? y)
        (fun i => List.nth i map_ptr c0)
        8 2 bpath in vbpath = true
    end.
Proof. reflexivity. Qed.
