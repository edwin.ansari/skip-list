Require Import ZArith.
From Coq Require Import Lists.List Bool.Bool.
Require Import FunInd Recdef.
Require Import Lia.
Scheme Equality for list.
 
Import ListNotations.
Set Implicit Arguments.

(* TODO 
  make a module param that defines _basis_
*)
Definition basis := 2.

Section Def.
  Variables (content_t ptr_t : Type).

  Record cell := mkCell
  { content : content_t;
    back_pointers : list ptr_t;
    index : nat;
  }.

  (* aux functions *)
  Definition eq_opt {A : Type}
   (eq : A -> A -> bool)
   (o1 o2 : option A) :
    bool :=
      match o1, o2 with
      | None, None => true
      | Some e1, Some e2 => eq e1 e2 
      | _, _ => false
    end.

  (* 
    preconditions:
      acc = 0
      r = false
    postconditions:
      return ceil (log_basis n) if n > 0
      return 0 if n = 0
    * r = false iff n = basis^i
  *)
  Function log_basis_aux (n acc : nat) (r : bool)
    {wf lt n} : nat :=
      match n with 
      | 0 | S 0 => acc + if r then 1 else 0
      | _ =>
        log_basis_aux (n / basis) (acc + 1)
        (r || ( 0 <? (n mod basis)))
      end.
      Proof.
        intros.
        - apply Nat.div_lt.
          + apply Nat.lt_0_succ.  
          + apply Nat.lt_succ_diag_r.
        - apply lt_wf.    
      Defined.

  (* return ceil (log_basis n) *)
  Definition log_basis (n : nat) : nat :=
    log_basis_aux n 0 false. 

  Fixpoint add_last {A: Type}
    (l : list A) (a : A): list A :=
    match l with 
    | [] => [a]
    | x :: xs => x :: add_last xs a 
    end.
  
  (* interface implementation *)
  Definition genesis 
    (ct : content_t)
    : cell :=
      mkCell ct [] 0.

  Definition back_pointer
    (c : cell)
    (i : nat)
    : option ptr_t :=
      List.nth_error (back_pointers c) i.

  Definition equal_back_pointers {ptr_t : Type}
    (equal_ptr : ptr_t -> ptr_t -> bool)
    (b1 b2 : list ptr_t)
    : bool :=
      list_beq ptr_t equal_ptr b1 b2.

  Definition equal
    (equal_content : content_t -> content_t -> bool)
    (equal_ptr : ptr_t -> ptr_t -> bool)
    cell1 cell2
    : bool :=
      equal_content (content cell1) (content cell2) &&
      (index cell1 =? index cell2) &&
      equal_back_pointers equal_ptr 
      (back_pointers cell1) (back_pointers cell2).
  
  (* invariant to prove: @return never None. because this
    means that no fuel remains to calculate the expected value 
    preconditions:
      init_power = 1
      init_acc = []
      init_i = 0
      init_fuel > log_basis index, so its sufficient to have,
      init_fuel > index in general.
  *)
  Fixpoint next_back_pointers_opt
    (prev_cell : cell)
    (prev_cell_ptr : ptr_t)
    (index : nat)
    (power : nat)
    (acc : list ptr_t)
    (i : nat)
    (fuel : nat)
    : option (list ptr_t) := 
      match fuel with 
      | 0 => None
      | S n => 
        if index <? power then Some (List.rev acc)
        else
          let back_pointer_i :=
            if index mod power =? 0 then
              Some prev_cell_ptr
            else back_pointer prev_cell i 
          in
          match back_pointer_i with 
          | None => None 
          | Some bp =>
            let acc := bp :: acc in 
            next_back_pointers_opt
              prev_cell prev_cell_ptr index 
              (power * basis) acc (i + 1) (n)
      end end.

  Definition next_back_pointers_safe
    (prev_cell : cell)
    (prev_cell_ptr : ptr_t)
    (index : nat)
    : (list ptr_t) :=
      let fuel := index + 1 in 
      let bptrs_opt := 
        next_back_pointers_opt
          prev_cell prev_cell_ptr index 
          1 [] 0 fuel 
      in
      match bptrs_opt with 
      | None => [] (* should prove that never happens *)
      | Some bptrs => bptrs
      end.
    
  Definition next
    (prev_cell : cell)
    (prev_cell_ptr : ptr_t)
    (ct : content_t)
    : cell :=
      let next_index := index prev_cell + 1 in
      let next_back_ptrs := 
        next_back_pointers_safe
          prev_cell prev_cell_ptr next_index in 
      let next_cell := mkCell ct next_back_ptrs next_index in
        next_cell.

  (* equivalent and simpler version of `next`, not tail-recursive *)
  Fixpoint next_back_pointers'
    (bptrs : list ptr_t)
    (prev_cell_ptr : ptr_t)
    (index : nat)
    (power : nat)
    (i : nat)
    : (list ptr_t) := 
      if index <? power then [] else
      match bptrs with 
      | [] => []
      | bptr :: xs =>
        (if index mod power =? 0 then prev_cell_ptr else bptr) ::
          next_back_pointers' xs prev_cell_ptr index (power * basis) (i+1)
      end.

  Definition next'
    (prev_cell : cell)
    (prev_cell_ptr : ptr_t)
    (ct : content_t)
    : cell :=
      let next_index := index prev_cell + 1 in
      let bptrs := 
        add_last (back_pointers prev_cell) prev_cell_ptr in
      let next_bptrs := 
          (next_back_pointers'
            bptrs prev_cell_ptr next_index 1 0) in 
      let next_cell := mkCell ct next_bptrs next_index in
        next_cell.

  Definition pointed_cell_index (src_index src_bptr_idx : nat) : nat :=  
    src_index - (src_index mod Nat.pow basis src_bptr_idx) - 1.

  Definition pointed_cell_index_p (src_index src_bptr_idx p : nat) : nat :=
    src_index - (src_index mod p) - 1.

  (* find the best(closest)-skip to the [target_index]
    in respect of the following formula:
    ** 
      [i]-th back_pointer of [cell] points to the element with following index
      [cell_index - (cell_index mod basis^i) - 1] in the list.
    **
    here [p] is an accumulator parameter to calculate [basis^i] 
    invariants:
      r = len - i
    preconditions:
      init_i = 0
      init_r = len
      p = 1
      where len = length (cell.back_pointers) ~= log_basis (cell.index)
  *)
  Fixpoint best_skip_aux
  (cell_index target_index i p r: nat)
  (best_idx : option nat)
  : option nat :=
    match r with 
    | 0 => best_idx
    | S pred_r => 
      let p_idx := pointed_cell_index_p cell_index i p in
        if p_idx <? target_index then best_idx
        else 
          best_skip_aux cell_index target_index
            (i+1) (basis * p) (pred_r) (Some i) 
    end.

  (* find an index of back_ptrs of [c]
    which that the associated ptr_i (back_ptrs[index])
    is the right-way closest to [target_index], more formally:
    max{i in Nat | target_index < (c.index - c.index mod basis^i - 1) }
  *)
  Definition best_skip
    (c : cell)
    (target_index : nat) 
    : option nat :=
      let cell_index := index c in
      let len := List.length (back_pointers c) in 
      if cell_index <? target_index then None 
      else
        best_skip_aux cell_index target_index 0 1 len None.

  Function back_path_aux
    (deref : ptr_t -> cell)
    (ptr : ptr_t)
    (target_index : nat)
    (path : list ptr_t)
    (skip : nat)
    {wf lt skip}
    : option (list ptr_t) :=
      let path := ptr :: path in 
      let c := deref ptr in 
      let c_index := index c in 
        match skip with 
        | 0 => Some (List.rev path)
        | S _ =>
        if c_index =? target_index then Some (List.rev path)
        else if c_index <? target_index then None 
        else
          match (best_skip c target_index) with
          | None => None 
          | Some best_ptr_index => 
            match (back_pointer c best_ptr_index) with 
            | None => None 
            | Some best_ptr => 
              let delta := c_index - index (deref best_ptr) in 
                match delta with 
                | 0 => None (* never happens *)
                | S _ => 
                  back_path_aux deref best_ptr target_index
                    path (skip - delta) 
        end end end end.
      Proof.
        - lia.
        - apply lt_wf.
      Defined.

  Definition back_path 
    (deref : ptr_t -> cell)
    (cell_ptr : ptr_t)
    (target_index : nat)
    : option (list ptr_t) :=
      let skip := index (deref cell_ptr) - target_index in 
        back_path_aux deref cell_ptr target_index
          [] skip.

  Definition mem {ptr_t}
    (equal : ptr_t -> ptr_t -> bool)
    (x : ptr_t)
    (l : list ptr_t)
    : bool :=
      List.existsb (equal x) l.

  Definition assume_some {A}
    (o : option A)
    (f : A -> bool)
    : bool :=
      match o with None => false | Some x => f x
      end.
  
  Fixpoint valid_path
    (equal_ptr : ptr_t -> ptr_t -> bool)
    (deref : ptr_t -> cell)
    (target_ptr : ptr_t)
    (c_index : nat)
    (cell_ptr : ptr_t)
    (path : list ptr_t)
    : bool :=
      let target_index := index (deref target_ptr) in 
        match (cell_ptr, path) with 
        | (final_cell, []) =>
          (equal_ptr target_ptr final_cell) && (c_index =? target_index)
        | (cell_ptr, cell_ptr' :: path) =>
          let c_cell := deref cell_ptr in 
          let c_cell' := deref cell_ptr' in 
          mem equal_ptr cell_ptr' (back_pointers c_cell) && 
          (
            match best_skip c_cell target_index with 
            | None => false 
            | Some best_idx =>
              match back_pointer c_cell best_idx with 
              | None => false 
              | Some best_ptr => 
                let minimal := equal_ptr best_ptr cell_ptr' in 
                let index' := index c_cell' in 
                minimal &&
                 (valid_path equal_ptr deref target_ptr
                  index' cell_ptr' path)
            end end
          ) end.

  Definition valid_back_path
    (equal_ptr : ptr_t -> ptr_t -> bool)
    (deref : ptr_t -> cell)
    (cell_ptr : ptr_t)
    (target_ptr : ptr_t)
    (path : list ptr_t)
    : bool :=
      let c := deref cell_ptr in 
      let target := deref target_ptr in 
      let target_index := index target in 
      let c_index := index c in 
        match path with 
        | [] => false 
        | first_cell_ptr :: path =>
          equal_ptr first_cell_ptr cell_ptr &&
          valid_path equal_ptr deref target_ptr c_index cell_ptr path
        end.

End Def.
Arguments genesis [content_t ptr_t].
