
include Stdlib.List
let hd = function x :: _ -> Some x | [] -> None

let rec assoc ~equal k = function
  | [] -> None
  | (kk, v) :: kvs -> if equal k kk then Some v else assoc ~equal k kvs